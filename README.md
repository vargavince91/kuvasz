# Kuvasz

This is an [open-source](LICENSE) Chrome extension that lets you handle various languages easily on Google Translate by providing a simple way to highlight the languages you use most often.

![Example screenshot](assets/highlight-classes.png)


## Usage

### Basic User Interface

* **`Ctrl+Left Click`** to toggle the highlight of the language and use the vivid, blue background color.
* **`Shift+Ctrl+Left Click`** to highlight with the default googley, grey background.

### Graphical User Interface

*Coming thoon*

![Phteven says it's coming soon](https://s-media-cache-ak0.pinimg.com/236x/ee/82/bd/ee82bd737b64136dfd9a2027fd2ff228.jpg)


## Tests

I use [Karma](http://karma-runner.github.io/) test runner and [Jasmine](http://jasmine.github.io/) testing framework for unit tests.

* Run tests locally:
    ```
    $ npm test
    ```

* Run tests inside a Docker container with headless Chrome [`chromium-xvfb-js`](https://hub.docker.com/r/markadams/chromium-xvfb-js/). The Docker image has [`xvfb`](https://en.wikipedia.org/wiki/Xvfb), Chrome, Node.js and NPM installed (See [`Dockerfile`](https://hub.docker.com/r/markadams/chromium-xvfb-js/~/dockerfile/)). `xvfb` *(x virtual framebuffer)*, performs all graphical operations in memory without showing any screen output, perfect for headless testing in containers.
    ```
    $ docker build -t kuvasz-karma .
    $ docker run kuvasz-karma
    ```

* Continuous Integration is set up and tests are run on [shared GitLab Runners](https://about.gitlab.com/2016/04/05/shared-runners/)

*At the moment, I only have unit tests, functional tests are missing. I plan to add functional tests using Selenium after the graphical user interface is implemented.*


## Images

* [Project Avatar's Image By Kuvaszprince `http://kuvaszprince.com.br, CC BY-SA 3.0`](https://commons.wikimedia.org/w/index.php?curid=5985029)
