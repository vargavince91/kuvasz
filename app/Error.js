(function(win, app){
    // The number of possible JS Custom Error Type implementations are endless.
    // http://stackoverflow.com/questions/1382107/whats-a-good-way-to-extend-error-in-javascript
    // https://bugs.chromium.org/p/chromium/issues/detail?id=228909
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
    function KuvaszExtensionError(message) {
        this.name = 'KuvaszExtensionError';
        this.message = message;
        this.stack = (new Error()).stack;
    }
    KuvaszExtensionError.prototype = new Error;

    app.KuvaszExtensionError = KuvaszExtensionError;

})(window, window.kuvasz=window.kuvasz || {});
