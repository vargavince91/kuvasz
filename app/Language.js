(function (app) {

    var Language = function (code, name, highlight) {
        this.code = code;
        this.name = name;
        this.highlight = highlight;
        this.$right = null;
        this.$left = null;
    };

    Language.googHighlight = "goog-menuitem-emphasize-highlight";
    Language.defaultHighlight = "kuvasz-menuitem-highlight-1";

    Language.prototype.addDomElement = function (domElement, side) {
        if (!(domElement instanceof Element)) {
            throw new app.KuvaszExtensionError("Invalid `domElement` parameter in `Language#addDom`. Expected instance of Node. Received: " + domElement);
        }
        if (!['left', 'right'].includes(side)) {
            throw new app.KuvaszExtensionError("Invalid `side` parameter in `Language#addDom`. Received: " + side);
        }
        side = '$' + side;
        this[side] = domElement;
        domElement.classList.add(this.highlight);
    }

    Language.prototype.handleHighlightEvent = function (event) {
        if (event.ctrlKey) {
            if (event.shiftKey) {
                this.changeHighlight(Language.googHighlight);
            } else {
                this.changeHighlight(Language.defaultHighlight);
            }
        }
    }

    Language.prototype.changeHighlight = function (newClass) {
        this.$right && this.$right.classList.remove(this.highlight);
        this.$left && this.$left.classList.remove(this.highlight);
        if (newClass !== this.highlight) {
            this.highlight = newClass;
            this.$right && this.$right.classList.add(this.highlight);
            this.$left && this.$left.classList.add(this.highlight);
        } else {
            this.highlight = null;
        }
        this._persist();
    }

    Language.prototype._persist = function () {
        var toBeSaved = {};
        toBeSaved[this.code] = this.highlight;
        chrome.storage.sync.set(toBeSaved, function () {
            console.log("Highlight choice persisted!", toBeSaved, this);
        }.bind(this));
    }

    // export as:
    app.Language = Language;

})(window.kuvasz=window.kuvasz || {});
