(function(app){
    var Language = app.Language;

    var LanguageList = function () {
        this._list = [];
        this._highlighted = {};

        // Has to be loaded only once, we take care of updating it manually
        chrome.storage.sync.get(null, function (result) {
            this._highlighted = result;
        }.bind(this));
    };

    LanguageList.prototype.getAll = function () {
        return this._list;
    };

    LanguageList.prototype.add = function (code, name) {
        var language = new Language(code, name, this._isHighlighted(code));
        this._list.push(language);
    };

    LanguageList.prototype.addDomElement = function (languageName, domElement, side, retry) {
        var language = this.get("name", languageName);
        // Bit hacky, but: we have Chinese on the left side menu
        // and Chinese (Simplified)/Chinese (Traditional) on the right side menu
        if (language === null && !retry) {
            languageName = languageName.split(" (");
            return this.addDomElement(languageName[0], domElement, side, true);
        }
        language && language.addDomElement(domElement, side);
        return language;
    };

    LanguageList.prototype.get = function (p, value) {
        for (var i = 0; i < this._list.length; i++) {
            var language = this._list[i];
            if (language[p] === value) {
                return language;
            }
        }
        return null;
    };

    LanguageList.prototype._isHighlighted = function (code) {
        return this._highlighted[code] ? this._highlighted[code] : null;
    }

    // export as:
    app.LanguageList = LanguageList;

})(window.kuvasz=window.kuvasz || {});
