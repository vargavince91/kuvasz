/**
 * The `allIndexOf` namespace includes code that extends the built-in String
 * object with the `allIndexOf()` method.
 *
 * Read more about extending built-in object on {@link https://www.kirupa.com/html5/extending_built_in_objects_javascript.htm | kirupa.com}
 * or on {@link http://stackoverflow.com/questions/8859828/javascript-what-dangers-are-in-extending-array-prototype | StackOverflow}.
 *
 * @namespace allIndexOf
 */
(function (app, undefined) {
    if (app && app.utils && app.utils.allIndexOf) {
        // Running it twice would break reverting the string object extension.
        return;
    }

    /**
     * The allIndexOf() method returns an array of indices within the calling
     * String object of all occurrences of the specified value, starting
     * the search at fromIndex. Returns an empty array if the value is not found.
     *
     * Inspired by {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf | String.indexOf}.
     *
     * @memberOf allIndexOf
     * @method _allIndexOf
     * @param {String} searchValue - A string representing the value to search for.
     * @param {Number} [fromIndex] - The index at which to start the searching
     * forwards in the string. Default value: 0.
     * @exception throws an error if the `searchValue` argument is empty string.
     * @returns {Array} Indices of all occurrences of the specified value
     * within the calling String object. Empty array, if value not found.
     */
    var _allIndexOf = function (searchValue, fromIndex) {
        if (searchValue === "") {
            throw "`searchValue` must not be empty string!"
        }
        var indices = [];
        var index = this.indexOf(searchValue, fromIndex);
        while (index !== -1) {
            indices.push(index);
            fromIndex =  index + 1;
            index = this.indexOf(searchValue, fromIndex);
        }
        return indices;
    };

    /**
     * A variable to store the original method for `String.allIndexOf`.
     * It is added so extending the String object can be reverted by calling the
     * `{@link allIndexOf.disableStringAllIndexOf}`.
     * @memberOf allIndexOf
     */
    var _overwrittenAllIndexOf = null;

    /**
     * Extend the standard built-in String object with `allIndexOf` method.
     * For more information see `{@link allIndexOf._allIndexOf}`.
     * @memberOf allIndexOf
     * @example
     * "asdasd".allIndexOf("a")  // [0, 3];
     */
    var enableStringAllIndexOf = function  () {
        // called first time, save original method to _overwrittenAllIndexOf
        if (_overwrittenAllIndexOf === null) {
            _overwrittenAllIndexOf = String.prototype.allIndexOf;
        }

        if (_overwrittenAllIndexOf !== undefined &&
            _overwrittenAllIndexOf !== _allIndexOf) {
            console.warn("Overwriting implementation of `allIndexOf` method " +
                         "of String standard built-in object. You can revert " +
                         "this by calling `disableStringAllIndexOf()`");
        }

        String.prototype.allIndexOf = _allIndexOf;
    };

    /**
     * Revert (disable) adding `allIndexOf` method to the built-in String object.
     * Uses variable `{@link allIndexOf._overwrittenAllIndexOf}` to read
     * original implementation from.
     *
     * @memberOf allIndexOf
     */
    var disableStringAllIndexOf = function () {
        if (_overwrittenAllIndexOf !== null) {
            String.prototype.allIndexOf = _overwrittenAllIndexOf;
        } else {
            console.warn("`enableStringAllIndexOf` has never been called.")
        }
        _overwrittenAllIndexOf = null;
    };

    // export to window.kuvasz.utils
    app.utils = app.utils || {};
    app.utils.allIndexOf = {
        enableStringAllIndexOf: enableStringAllIndexOf,
        disableStringAllIndexOf: disableStringAllIndexOf
    };
})(window.kuvasz=window.kuvasz || {});
