(function(app){
    var utils = app.utils;

    utils.allIndexOf.enableStringAllIndexOf();

    var languageList = new app.LanguageList();

    utils.httpGet(window.location.href, function (response) {
        app.initLanguageList(languageList, response);
        app.enableListeners(languageList);
    });

    //chrome.storage.sync.clear();

})(window.kuvasz=window.kuvasz || {});
