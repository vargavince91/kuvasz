(function (app){

    var ID = {
        menu: {
            left: "gt-sl-gms-menu",
            right: "gt-tl-gms-menu"
        },
        button: {
            left: "gt-sl-gms",
            right: "gt-tl-gms"
        }
    };


    var $kuvasz = {
        button: {
            left: document.getElementById(ID.button.left),
            right: document.getElementById(ID.button.right)
        },
        // DOM elements created only when button clicked.
        menu: {
            left: null,
            right: null
        }
    };


    var _languageList = null;


    function enableListeners (languageList) {
        _languageList = languageList;
        $kuvasz.button.left
            .addEventListener("click", extendLanguagesWithDom, false);
        $kuvasz.button.right
            .addEventListener("click", extendLanguagesWithDom, false);

        // TODO: remove it and refactor this into a proper button when GUI added
        // event listener added, just to make debugging easier.
        $kuvasz.button.right.addEventListener("click", function (event) {
            if (event.ctrlKey) {
                chrome.storage.sync.clear(function () {
                    console.log("Storage Clear. Success.");
                });
            }
        }, false);
    }


    function extendLanguagesWithDom (event) {
        event.currentTarget
                .removeEventListener("click", extendLanguagesWithDom, false);

        var side;
        if (event.currentTarget.id === ID.button.left) {
            side = "left";
        } else if (event.currentTarget.id === ID.button.right) {
            side = "right";
        } else {
            throw new app.KuvaszExtensionError("Couldn't match ID #" + event.currentTarget.id + " to any of the dropdown buttons.");
        }

        $kuvasz.menu[side] = document.getElementById(ID.menu[side]);

        var optionsNodeList = $kuvasz.menu[side]
                .querySelectorAll('.goog-menuitem.goog-option');

        [].forEach.call(optionsNodeList, function (element) {
            var name = element.firstChild.innerHTML.split("</div>")[1];
            var language = _languageList.addDomElement(name, element, side);
            // Normally `this` would be the DOM element. This is not what we want,
            // we want the `this` in the `language.handleHighlightEvent` method
            // to be the language.
            // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_objects/Function/bind
            // http://stackoverflow.com/questions/1338599/the-value-of-this-within-the-handler-using-addeventlistener
            element.addEventListener("click", language.handleHighlightEvent.bind(language), false);
        });
    }


    app.enableListeners = enableListeners;


})(window.kuvasz=window.kuvasz||{});
