(function(app){
    /**
     * Remove opening and closing `select` tags
     * (includes their attributes and classes) from a string.
     * @param {String} str - The string the select tags has to removed from.
     * @returns {String} The input string without select tags.
     */
    function _removeSelectTags (str) {
        return str.replace(/<\/?select.*?>/g, "");
    }

    /**
     * Map the language codes to the language's names in the user's language.
     * We need the language codes, so the user's preferences can be persisted
     * by language code and the highlighted languages can be restored
     * across all sites (say translate.google.com and translate.google.com.mx).
     * **Note**: all this mambo-jambo is necessary because the language selector
     * menu elements don't have ids or any attributes that creates one-to-one
     * relations that work across different domains of Google Translate.
     * @param {String} - A string containing a list of option tags.
     * @returns {Object} - An object (think of it as key-value store) where the
     * keys are language codes and the values are language names in the
     * user's language.
     */
    function _mapLanguageCodeName (options) {
        var indexes = options.allIndexOf("<option");
        var map = {};

        for (var i = 0; i < indexes.length; i++) {
            var x = options.substring(indexes[i], indexes[i+1]);
            // x == "<option value=de>German</option>"
            x = x.split("value=")[1];
            // x == "de>German</option>"
            x = x.split("</option>")[0]
            // or x = x.substring(0, x.length-"</option>".length);
            // x == "de>German"
            x = x.split(">");
            // x == ["de", "German"]
            if (x.length !== 2) {
                throw new app.KuvaszExtensionError("Can't map locales and their codes");
            }
            map[x[0]] = x[1];
        }

        return map;
    }

    /**
     * Initialize a `{@link LanguageList}` instance with languae codes and names.
     * @param {String} response - A response we get when making a `GET` request
     * to any of the Google Translate domains.
     */
    function initLanguageList (languageList, response) {
        var start = response.allIndexOf("<select id=gt-sl");
        if (start.length !== 1) {
            throw new app.KuvaszExtensionError(
                    "Expected to find exactly one '<select id=gt-sl>'. " +
                    "Found: " + start.length);
        }
        start = start[0];
        // get select option tags
        var select = response.substring(start, response.indexOf('</select>') + 9);
        // remove select tags, keep options only
        var options = _removeSelectTags(select);
        var mapLanguages = _mapLanguageCodeName(options);
        for (var key in mapLanguages) {
            // don't need to check hasOwnProperty, simple obj literal
            languageList.add(key, mapLanguages[key]);
        }
        return languageList;
    };

    app.initLanguageList = initLanguageList;

    // for testing
    app.Jasmine = app.Jasmine || {};
    app.Jasmine.initLanguageList = {};
    app.Jasmine.initLanguageList.removeSelectTags = _removeSelectTags;
    app.Jasmine.initLanguageList.mapLanguageCodeName = _mapLanguageCodeName;


})(window.kuvasz=window.kuvasz || {});
