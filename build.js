var fs = require("fs");
var path = require("path");

var ClosureCompiler = require('google-closure-compiler').compiler;
var concat = require('concat-files');

var lsAppFiles = require("./node_tooling/ls-app-files.js");



var appFolderPath = path.join(__dirname, "app");
var jsFiles = lsAppFiles.get(appFolderPath, ".js", true);
var cssFiles = lsAppFiles.get(appFolderPath, ".css", true);
var outputJs = path.join(__dirname, "dist/app.js");
var outputCss = path.join(__dirname, "dist/app.css");

var closureCompiler = new ClosureCompiler({
  js: jsFiles,
  compilation_level: 'SIMPLE',
  js_output_file: outputJs
});

closureCompiler.run(function(exitCode, stdOut, stdErr) {
    console.log("\n** CLOSURE COMPILER **");
    if (exitCode) {
        console.error("exitCode", exitCode);
        console.error("stdOut", stdOut);
        console.error("stdErr", stdErr);
    } else {
        console.log("Successfully minified JavaScript files:");
        jsFiles.forEach(function(x){console.log("  " + x);});
        console.log("Destination:");
        console.log("  " + outputJs);
    }
});

concat(cssFiles, outputCss, function(x) {
   console.log("\n** CONCAT CSS **");
   console.log("Successfully concatenated CSS files:");
   cssFiles.forEach(function(x){console.log("  " + x);});
   console.log("Destination:");
   console.log("  " + outputCss);
 });
