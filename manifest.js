/*
 * We use a `js` script and not directly editing the `manifest.json` file,
 * so it's more flexible, easier to change and maintain.
 * Accepts 'dist' flag to generate manifest file for 'dist' folder
 */



var fs = require("fs");
var path = require("path");

var lsAppFiles = require("./node_tooling/ls-app-files.js");
var translateDomains = require("./node_tooling/translate-domains.js");



var appFolderPath = path.join(__dirname, "app");



var DIST = process.argv[2] === "dist";
var destFolderPath, jsFiles, cssFiles;

if (DIST) {
    destFolderPath = path.join(__dirname, "dist");
    jsFiles = ["app.js"];
    cssFiles = ["app.css"];
} else {
    destFolderPath = appFolderPath;
    // Include all .js, app.js last
    jsFiles = lsAppFiles.get(appFolderPath, ".js");
    cssFiles = lsAppFiles.get(appFolderPath, ".css");
}



var manifest = {
    // https://developer.chrome.com/extensions/manifest
    /* REQUIRED FIELDS */

    // The format of this manifest vile is generally stable, but occasionally
    // breaking changes must be made to address important issues.
    "manifest_version": 2,

    // max of 45 characters
    // used: install dialog, extension management UI, Chrome Web Store
    "name": "Kuvasz Translate",

    // One to four dot-separated integers identifying
    // the version of this extension
    "version": "0.1.0",


    /* RECOMMENDED FIELDS */

    // max of 132 characters
    // used: extension management UI, Chrome Web Store
    "description": "An open-source extension that lets you handle various languages easily on Google Translate",

    // Specifies the subdirectory of _locales that contains
    // the default strings for this extension
    // * required, if we have a _locales directory, must be absent otherwise
    // https://developer.chrome.com/extensions/i18n
    // "default_locale": "en",

    // icons representing the extension
    // PNG recommended (best support for transparency)
     //"icons": {"128": "icon128.png"}

    "author": "Vince Varga",

    // max of 12 characters recommended
    // used: App launcher, New Tab page
    "short_name": "Kuvasz",

    "content_scripts": [
        {
            "matches": translateDomains.ALL,
            "js": jsFiles,
            "css": cssFiles
        }
    ],

    // To use most chrome.* APIs, our extension must declare its intent
    // https://developer.chrome.com/extensions/declare_permissions
    "permissions": [
        // optimized to meet the specific storage needs of extension
        // https://developer.chrome.com/extensions/storage
        "storage"
    ]

};


/* CREATE MANIFEST FILE */
var manifestJson = JSON.stringify(manifest, null, 4);
var manifestFileName = path.join(destFolderPath, "manifest.json");

fs.writeFile(manifestFileName, manifestJson, function(err) {
    if (err) {
        return console.error(err);
    }
    console.log("Manifest file created: " + manifestFileName);
});
