var fs = require("fs");
var path = require("path");



module.exports.get = function (appFolderPath, extension, fullPath) {
    var files = [];
    var appFiles = fs.readdirSync(appFolderPath);
    var appJs;

    appFiles.forEach(function (filename) {
        if (filename.endsWith(extension)) {
            // app.js comes last.
            if (filename !== "app.js") {
                files.push(filename);
            } else {
                appJs = filename;
            }
        }
    });

    if (appJs) {
        files.push(appJs);
    }

    if (fullPath) {
        return files.map(function (jsFile) {
            return path.join(appFolderPath, jsFile);
        });
    } else {
        return files;
    }
};
