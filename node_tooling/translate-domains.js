var fs = require("fs");
var path = require("path");

// Wildcards are not allowed in the TLD
// http://stackoverflow.com/questions/11069644/support-all-google-domains-in-a-content-script
// https://developer.chrome.com/extensions/match_patterns

module.exports.ALL = fs.readFileSync(path.join(__dirname, "supported_domains.txt"))
    .toString()
    .split(" ")
    .map(function(domain){
        // for example: ".google.com"
       return "*://translate" + domain + "/*";
    });
