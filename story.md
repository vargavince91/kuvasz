# Kuvasz `--verbose`

## What's this?

This is an [open-source](LICENSE) Chrome extension that lets you handle various languages easily on Google Translate by providing a simple way to highlight the languages you use most often.

![Example screenshot](assets/highlight-classes.png)

## What's a Kuvasz?

> The Kuvasz (Hungarian pronunciation: [ˈkuvɒs]), is an ancient breed of a livestock dog of Hungarian origin. It has historically been used as a royal guard dog, or to guard livestock, but has been increasingly found in homes as a pet over the last seventy years. The Kuvasz is a large dog with a dense double, odorless coat which is white in color and can range from wavy to straight in texture. Source: [Wikipedia - Kuvasz](https://en.wikipedia.org/wiki/Kuvasz)

![Kuvasz Puppy](assets/kuvasz.jpg)

## Why did you name this project after a dog?

I don't like spending hours thinking about the name of my next *pet* projects. I name my open-source projects after dog breeds.

## The story

As someone, who:

* is from Hungary
* lives in Germany, currently learning German
* works at an English-speaking company and spends hours reading English articles
* listens to Spanish music and watches Spanish comedians
* must read documents written in other languages

I need to use Google Translate several times a day.

Google Translate only highlights up to three languages and the highlight is really difficult to spot in a list of 100+ languages. With proper highlights, it's easy to switch between languages.

## The software stack

I don't have much experience with developing Chrome Extensions. However, I like discovering new technologies, solving problems and improving my coding skills.

> Dude, suckin' at something is the first step to being sorta good at something - Jake The Dog, Adventure Time

![Jake The Dog from Adventure Time](assets/jake.png)

## Images
* [Jake the Dog](https://en.wikipedia.org/wiki/File:JaketheDog.png) is a character from Adventure Time. The purpose of including the image here is to show the source of the inspirational quote
* [Project Avatar's Image By Kuvaszprince `http://kuvaszprince.com.br, CC BY-SA 3.0`](https://commons.wikimedia.org/w/index.php?curid=5985029)
