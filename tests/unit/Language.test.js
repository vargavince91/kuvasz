"use strict";

describe("Language", function() {

    var Language = window.kuvasz.Language;

    var language, $exampleElement;


    beforeEach(function () {
        // not highlighted: third param is null
        language = new Language("en", "inglés", null);
        $exampleElement = document.createElement("div");
        $exampleElement.innerHTML = "Inglés";
    });


    it("can be extended with DOM elements.", function () {
        language.addDomElement($exampleElement, "right");
        expect(language.$right).toBe($exampleElement);
    });


    it("accepts DOM elements representing the language", function () {
        var addDomElement = Language.prototype.addDomElement;
        // valid `side` parameter and DOM element
        expect(addDomElement.bind(language, $exampleElement, "right")).not.toThrow();
        expect(addDomElement.bind(language, $exampleElement, "left")).not.toThrow();
    });


    it("throws error when the side is not valid.", function () {
        var addDomElement = Language.prototype.addDomElement;
        // invalid `side` parameters
        expect(addDomElement.bind(language, $exampleElement, "up")).toThrow();
        expect(addDomElement.bind(language, $exampleElement, null)).toThrow();
        expect(addDomElement.bind(language, $exampleElement)).toThrow();
    });


    it("throws error when domElement is not really an Element", function () {
        var addDomElement = Language.prototype.addDomElement;
        // invalid `domElement` parameters
        expect(addDomElement.bind(language, "abc", "right")).toThrow();
        expect(addDomElement.bind(language, null, "right")).toThrow();
    });


    it("initializes highlight property in constructor.", function () {
        language = new Language("en", "inglés", Language.googHighlight);
        expect(language.highlight).toBe(Language.googHighlight);
    });


    describe("#changeHighlight", function() {

        beforeEach(function () {
            // just to make tests a bit cleaner
            $exampleElement.hasClass = function (cssClass) {
                return this.classList.contains(cssClass);
            }
        });


        it("toggles highlight property", function () {
            expect(language.highlight).toBe(null); // initial value
            // first time: add
            language.changeHighlight(Language.defaultHighlight);
            expect(language.highlight).toBe(Language.defaultHighlight);
            // second time, same arg: remove
            language.changeHighlight(Language.defaultHighlight);
            expect(language.highlight).toBe(null);
        });


        it("persists CSS classes", function () {
            spyOn(chrome.storage.sync, "set");
            // call it twice, "toggle" the class
            language.changeHighlight(Language.defaultHighlight);
            language.changeHighlight(Language.defaultHighlight);

            expect(chrome.storage.sync.set).toHaveBeenCalledTimes(2);
            // first time it's called
            expect(chrome.storage.sync.set.calls.argsFor(0)[0])
                .toEqual({"en": Language.defaultHighlight});
            // second time it's called, set to null
            expect(chrome.storage.sync.set.calls.argsFor(1)[0])
                .toEqual({"en": null});
            // callback would be argsFor(1)[1], but as it's just a console.log
            // it's unnecessary to test
        });


        it("adds CSS class to defined elements.", function () {
            // add Element to test the effect on
            language.addDomElement($exampleElement, "right");
            // initial value
            expect(language.highlight).toBe(null);
            expect(language.$right.hasClass(Language.defaultHighlight)).toBe(false);

            language.changeHighlight(Language.defaultHighlight);

            expect(language.highlight).toBe(Language.defaultHighlight);
            expect(language.$right.hasClass(Language.defaultHighlight)).toBe(true);
        });


        it("removes original CSS class from defined elements and " +
           "replaces it with new CSS class.", function () {
            language = new Language("en", "inglés", Language.defaultHighlight);
            language.addDomElement($exampleElement, "right");
            // initially we expect:
            expect(language.highlight).toBe(Language.defaultHighlight);
            expect(language.$right.hasClass(Language.defaultHighlight)).toBe(true);
            expect(language.$right.hasClass(Language.googHighlight)).toBe(false);

            language.changeHighlight(Language.googHighlight);

            expect(language.highlight).toBe(Language.googHighlight);
            expect(language.$right.hasClass(Language.defaultHighlight)).toBe(false);
            expect(language.$right.hasClass(Language.googHighlight)).toBe(true);
        });

    });


    describe("#handleHighlightEvent", function () {

        var event;


        beforeEach(function () {
            event = {};
            spyOn(language, "changeHighlight");
        });


        it("sets highlight to defaultHighlight, when clicked with Ctrl key", function () {
            event.ctrlKey = true;
            language.handleHighlightEvent(event);
            expect(language.changeHighlight).toHaveBeenCalledTimes(1);
            expect(language.changeHighlight).toHaveBeenCalledWith(Language.defaultHighlight);
        });


        it("sets highlight to google's highlight, when clicked with Ctrl+Shift key", function () {
            event.ctrlKey = true;
            event.shiftKey = true;
            language.handleHighlightEvent(event);
            expect(language.changeHighlight).toHaveBeenCalledTimes(1);
            expect(language.changeHighlight).toHaveBeenCalledWith(Language.googHighlight);
        });


        it("doesn't delegate events, when key kombo is not valid", function () {
            language.handleHighlightEvent(event);
            expect(language.changeHighlight).not.toHaveBeenCalled();

            event.shiftKey = true;
            language.handleHighlightEvent(event);
            expect(language.changeHighlight).not.toHaveBeenCalled();
        });

    });

});
