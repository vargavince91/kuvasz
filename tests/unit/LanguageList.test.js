"use strict";

describe("LanguageList", function () {

    var LanguageList = window.kuvasz.LanguageList;
    var Language = window.kuvasz.Language;

    var languageList;


    beforeEach(function () {
        languageList = new LanguageList();
        mockIsHighlighted();
        // Add two languages to the list
        languageList.add("en", "English");
        languageList.add("de", "German");
    });


    it("languages can be retrieved by any property.", function () {
        var english = languageList.get("code", "en");
        var german = languageList.get("name", "German");

        expect(english instanceof Language).toBeTruthy();
        expect(german instanceof Language).toBeTruthy();

        expect(english.name).toBe("English");
        expect(german.code).toBe("de");
    });


    it("initializes the language's highlight class", function () {
        // see mockIsHighlighted
        var english = languageList.get("code", "en");
        var german = languageList.get("name", "German");

        expect(english.highlight).toBe(null);
        expect(german.highlight).toBe(Language.defaultHighlight);
    });


    it("returns null, when language not found.", function () {
        expect(languageList.get("name", "Spanish")).toBeNull();
    });


    it("language information can be enriched by their representing DOM Elements.", function () {
        var $exampleElement = document.createElement("div");

        // could use _list, too, but this is how we would access it in the app
        var english = languageList.get("code", "en");
        spyOn(english, "addDomElement");
        var german = languageList.get("code", "de");
        spyOn(german, "addDomElement");

        languageList.addDomElement("German", $exampleElement, "left");

        expect(german.addDomElement).toHaveBeenCalledWith($exampleElement, "left");
        expect(english.addDomElement).not.toHaveBeenCalled();
    });


    function mockIsHighlighted () {
        spyOn(languageList, "_isHighlighted")
            .and
            .returnValues(null, Language.defaultHighlight);
    }

});
