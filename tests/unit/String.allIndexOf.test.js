describe("String#allIndexOf", function() {

    var app = window.kuvasz;

    // to not mess up other tests
    beforeEach(app.utils.allIndexOf.enableStringAllIndexOf);
    afterEach(app.utils.allIndexOf.disableStringAllIndexOf);


    it("returns empty array, if `searchValue` not found", function () {
        var emptyArray = [];
        expect("Toulouse".allIndexOf("x")).toEqual(emptyArray);
        expect("Toulouse".allIndexOf("t")).toEqual(emptyArray);
        expect("Toulouse".allIndexOf("toulouse")).toEqual(emptyArray);
        expect("Toulouse".allIndexOf(" ")).toEqual(emptyArray);
        expect("Toulouse".allIndexOf()).toEqual(emptyArray);
    });


    it("returns the same value (in an array) as `indexOf`, when the searched " +
       "value occurs exactly once in the searched string", function () {
        var str = "Thomas O'Malley";
        var testCases = ["T", "Th", " ", "'", "lley", "Thomas O'Malley"];
        for(var i = 0; i < testCases.length; i++) {
            var t = testCases[i];
            expect([str.indexOf(t)]).toEqualFor(str.allIndexOf(t), t);
        }
    });


    it("returns the indices of all occurences of `searchValue`", function() {
        expect("Cheetata".allIndexOf("a")).toEqual([5, 7]);
        expect("Cheetato".allIndexOf("e")).toEqual([2, 3]);
        expect("Cheetata and Cheetato".allIndexOf("Chee")).toEqual([0, 13]);
        expect("111".allIndexOf("1")).toEqual([0, 1, 2]);
    });


    it("only throws error if no `searchValue` has been specified", function () {
        expect(String.prototype.allIndexOf.bind("Marie", "")).toThrow();
        expect(String.prototype.allIndexOf.bind("Marie", "x")).not.toThrow();

        // expect("".allIndexOf).toThrow();  // true
        // Tests that it throws error under *some* circumstances
        // but can't tell what these circumstances are.
    });


    it("can handle exotic characters", function () {
        // kind of obvious, doe
        expect("Árvíztűrő".allIndexOf("Árvíztűrő"))
                .toEqual([0]);
        expect("Árvíztűrő".allIndexOf("Á"))
                .toEqual([0]);
        expect("Árvíztűrő".allIndexOf("ízt"))
                .toEqual([3]);
    });

});
