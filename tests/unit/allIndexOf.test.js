"use strict";


describe("app.utils.allIndexOf", function () {

    var app = window.kuvasz;


    beforeEach(cleanup);
    afterEach(cleanup);

    function cleanup () {
        // Need to clean up before and after each test
        // disable
        app.utils.allIndexOf.disableStringAllIndexOf();
        // remove manually
        String.prototype.allIndexOf = undefined;
    }


    it("adds and removes the `allIndexOf` method from String global object",
       function () {
        expect("Berlioz".allIndexOf).toBeFalsy();
        // enable / add method
        app.utils.allIndexOf.enableStringAllIndexOf();
        // Don't test algorithm correctness here
        expect("Berlioz".allIndexOf).toBeTruthy();
        // disable / remove method
        app.utils.allIndexOf.disableStringAllIndexOf();
        // see if method available (it shouldn't be anymore)
        expect("Berlioz".allIndexOf).toBeFalsy();
    });


    it("stores original String#allIndexOf implementation " +
       "that later can be restored", function () {
        // add (contrived) method
        String.prototype.allIndexOf = function () {
            return "potato";
        };
        expect("patato".allIndexOf("x")).toBe("potato");

        // enable
        app.utils.allIndexOf.enableStringAllIndexOf();
        expect("patato".allIndexOf("a")).toEqual([1, 3]);

        // restore original
        app.utils.allIndexOf.disableStringAllIndexOf();
        // contrived method kicks in!
        expect("patato".allIndexOf("x")).toBe("potato");
    });


    it("writes warning message, when `allIndexOf#disableStringAllIndexOf` is " +
       "called at the state where it's been already disabled.", function() {
        spyOn(console, 'warn');
        app.utils.allIndexOf.disableStringAllIndexOf();

        expect(console.warn).toHaveBeenCalledTimes(1);
    });


    it("writes warning message, when replaces other method", function() {
        spyOn(console, 'warn');

        // add other method
        var otherMethod = function () {};
        String.prototype.allIndexOf = otherMethod;
        expect(String.prototype.allIndexOf).toBe(otherMethod);

        // enable
        app.utils.allIndexOf.enableStringAllIndexOf();

        expect(String.prototype.allIndexOf).not.toBe(otherMethod);
        expect(console.warn).toHaveBeenCalledTimes(1);
    });

});
