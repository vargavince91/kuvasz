describe("initLanguageList", function () {

    var app = window.kuvasz;
    beforeEach(app.utils.allIndexOf.enableStringAllIndexOf);
    var initLanguageList = app.initLanguageList;


    it("fills the passed in languageList " +
       "with languages parsed from the response", function () {
           var languageList;
           languageList = new app.LanguageList();

           var r = '<!DOCTYPE html><html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><link rel=dns-prefetch href=//ssl.gstatic.com><link rel=dns-prefetch href=//csi.gstatic.com><link rel=dns-prefetch href=//www.google.com><meta name=keywords content="translate, translations, translation, translator, machine translation, online translation"><meta name=description content="Google&#39;s free service instantly translates words, phrases, and web pages between English and over 100 other languages. English, Afrikaans, Albanian, Amharic, Arabic, Armenian, Azerbaijani, Basque, Belarusian, Bengali, Bosnian, Bulgarian, Catalan, Cebuano, Chichewa, Chinese, Corsican, Croatian, Czech, Danish, Dutch, Esperanto, Estonian, Filipino, Finnish, French, Frisian, Galician, Georgian, German, Greek, Gujarati, Haitian Creole, Hausa, Hawaiian, Hebrew, Hindi, Hmong, Hungarian, Icelandic, Igbo, Indonesian, Irish, Italian, Japanese, Javanese, Kannada, Kazakh, Khmer, Korean, Kurdish (Kurmanji), Kyrgyz, Lao, Latin, Latvian, Lithuanian, Luxembourgish, Macedonian, Malagasy, Malay, Malayalam, Maltese, Maori, Marathi, Mongolian, Myanmar (Burmese), Nepali, Norwegian, Pashto, Persian, Polish, Portuguese, Punjabi, Romanian, Russian, Samoan, Scots Gaelic, Serbian, Sesotho, Shona, Sindhi, Sinhala, Slovak, Slovenian, Somali, Spanish, Sundanese, Swahili, Swedish, Tajik, Tamil, Telugu, Thai, Turkish, Ukrainian, Urdu, Uzbek, Vietnamese, Welsh, Xhosa, Yiddish, Yoruba, Zulu"><meta name=robots content=noodp><meta name=google content=notranslate><link rel="canonical" href="https://translate.google.com/?tl=hu"><title>Google Translate</title><link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml?hl=en" title="Google Translate"><script>JS_ERR_COUNT = 0;JS_ERR_ARR = [];JS_LOADED = false;</script>';
           r += '<label for=gt-sl class="gt-lang-lbl nje"></label>';
           r += '<select id=gt-sl name=sl class="jfk-button jfk-button-standard nje" tabindex=0>';
           r += '<option SELECTED value=en>English</option>';
           r += '<option value=hu>Hungarian</option>';
           r += '<option value=xh>Xhosa</option>';
           r += '<option value=yi>Yiddish</option>';
           r += '<option value=yo>Yoruba</option>';
           r += '<option value=zu>Zulu</option>';
           r += '</select>';
           r += '<div id="gt-tl-gms" class="goog-inline-block goog-flat-menu-button goog-flat-menu-button-collapse-left gt-gms-icon je">';

           initLanguageList(languageList, r);

           var xhosa = languageList.get("name", "Xhosa");
           expect(xhosa).not.toBeNull();
           expect(xhosa.code).toBe("xh");
           expect(xhosa.highlight).toBe(null);

           expect(languageList.getAll().length).toBe(6);
    });


    /* Here we test private methods which needs more unit testing
     * than is practical through a class's public interfaces.
     * http://stackoverflow.com/questions/34571/how-to-test-a-class-that-has-private-methods-fields-or-inner-classes
     */
    describe("#removeSelectTags", function () {
        var removeSelectTags = app.Jasmine.initLanguageList.removeSelectTags;

        it("removes simple select tags", function () {
            var str = "<p>x<select><option value='bb'>Billy Boss</option></select>y</p>";

            expect(removeSelectTags(str))
                .toBe("<p>x<option value='bb'>Billy Boss</option>y</p>");
        });


        it("removes select tags with attributes", function () {
            var str = "<p>x<select id='ac'><option value='hc'>Hit Cat</option></select>y</p>";

            expect(removeSelectTags(str))
                .toBe("<p>x<option value='hc'>Hit Cat</option>y</p>");
        });
    });


    describe("#mapLanguageCodeName", function () {
        var mapLanguageCodeName = app.Jasmine.initLanguageList.mapLanguageCodeName;

        it("creates a ~map (JavaScript Object Literal) of language codes and names", function () {
            var options = "<option SELECTED value=en>English</option>";
            options += "<option value=hu>Hungarian</option>";
            options += "<option value=zu>Zulu</option>";

            var expectedMap = {
                en: "English",
                hu: "Hungarian",
                zu: "Zulu"
            };

            expect(mapLanguageCodeName(options)).toEqual(expectedMap);
        });
    });

});
