"use strict";

beforeEach(function () {
    jasmine.addMatchers({
        /* We create something similar to the `toEqual` matcher.
         * https://github.com/jasmine/jasmine/blob/master/src/core/matchers/toEqual.js
         *
         * This matcher helps if
         * - we have several test cases that we wan't to include in a 'it' block
         *   and we loop over the test cases, so the line number won't help
         *   (whether one should use a loop in a test in a different question)
         * - the message of `toEqualFor` is easier to digest and
         *   the cause of the error (failing test) is easier to spot, than
         *   the error message of `toEqual`, even if we don't use loops in test
         */
        toEqualFor: function (util, customEqualityTesters) {
            customEqualityTesters = customEqualityTesters || {};
            return {
                compare: function (actual, expected, forValue) {
                    // Set defaults
                    if (typeof expected === "undefined") {
                        expected = "";
                    }
                    var result = {
                        pass: false
                    };
                    // Check equality
                    result.pass = util.equals(actual, expected, customEqualityTesters);
                    // Create messages
                    if (result.pass) {
                        // when the expectation is used with `.not`
                        result.message =
                                "For " + JSON.stringify(forValue) + ": " +
                                "Expected " + JSON.stringify(actual) +
                                " not to equal " + JSON.stringify(expected);
                    } else {
                        result.message =
                                "For " + JSON.stringify(forValue) + ": " +
                                "Expected " + JSON.stringify(actual) +
                                " to equal " + JSON.stringify(expected);
                    }
                    return result;
                }
            };
        }
    });
});
